# Move default router

```
oc edit ingresscontroller default -n openshift-ingress-operator -o yaml
```

```
spec:
  nodePlacement:
    nodeSelector:
      matchLabels:
        node-role.kubernetes.io/infra: ""
```

## Watch ingress router pods

```
oc get pods -n openshift-ingress -o wide
```