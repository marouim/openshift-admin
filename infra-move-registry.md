# Move default registry

```
oc edit configs.imageregistry.operator.openshift.io cluster
```

```
nodeSelector:
  node-role.kubernetes.io/infra: ""
```

## Watch image registry pods 

```
oc get pods -n openshift-image-registry -o wide
```
