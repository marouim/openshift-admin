1 - Create machine-set infra and OCS

2 - Explore machine-set interaction with Cloud provider

3 - Configure components to use new infra nodes

4 - Install Logging stack 

5 - Explore Volume interactions and limitations

  5.1 Install App
  https://gitlab.com/marouim/simpleproject-products-api.git
  
  5.2 Attach volume using standard cloud

6 - Install Storage stack

7 - Explore volume new capabilities

  7.1 Attach volume using OCS

8 - Add machine set for specific app 

  8.1 Move app to specific nodes
