# Move monitoring solution

```
oc apply -f infra-monitoring.yaml
```

## Watch monitoring pods

```
oc get pod -n openshift-monitoring -o wide
```
